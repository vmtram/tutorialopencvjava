package com.compa.opencv.camera;

import static org.opencv.imgproc.Imgproc.COLOR_BGR2GRAY;
import static org.opencv.imgproc.Imgproc.INTER_CUBIC;
import static org.opencv.imgproc.Imgproc.THRESH_BINARY;
import static org.opencv.imgproc.Imgproc.cvtColor;
import static org.opencv.imgproc.Imgproc.threshold;

import java.awt.Image;
import java.util.Scanner;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.highgui.VideoCapture;
import org.opencv.imgproc.Imgproc;

import com.compa.opencv.ImgShow;

/**
 * 
 * @author Sea
 *
 */
public class OpencvFirstStep {

	static ImgShow imgShowOrigin = new ImgShow("Origin");
	static String srcPath = "F:/opencv_camera/";

	static {
		// Load Opencv native library
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
	}

	/**
	 * Thread process camera
	 * 
	 * @author Sea
	 *
	 */
	class ThreadCapture extends Thread {
		VideoCapture camera;
		boolean isStop = false;
		boolean isCapture = false;

		public ThreadCapture(VideoCapture camera) {
			this.camera = camera;
		}

		@Override
		public void run() {
			Mat frame = new Mat();
			while (!isStop) {
				if (camera.read(frame)) {
					// Imgproc.resize(frame, frame, new Size(400,
					// frame.size().height * 400 / frame.size().width), 1.0,
					// 1.0, INTER_CUBIC);
					if (isCapture) {
						isCapture = false;
						Highgui.imwrite(srcPath + "1.png", frame);
					}
					imgShowOrigin.show(frame);
				}
			}
			imgShowOrigin.frame.setVisible(false);
			imgShowOrigin.frame.dispose();
			camera.release();

		}

		public void setStop(boolean stop) {
			isStop = stop;
		}
	}

	public static void main(String[] args) {
		System.out.println("Start app...");
		// openImage();
		openCamera();
		System.out.println("Done!");
	}

	/**
	 * Read image
	 */
	public static void openImage() {
		Mat m = Highgui.imread(srcPath + "img1.jpg");
		Size imgSize = m.size();
		Imgproc.resize(m, m, new Size(500, imgSize.height * 500 / imgSize.width), 1.0, 1.0, INTER_CUBIC);
		cvtColor(m, m, COLOR_BGR2GRAY);
		threshold(m, m, 100, 255, THRESH_BINARY);
		imgShowOrigin.show(m);
	}

	/**
	 * Start camera by index
	 * 
	 * @param cameraIndex
	 */
	public static void openCamera() {
		int cameraIndex = 0;
		System.out.println("Open camera " + cameraIndex);
		VideoCapture camera = new VideoCapture(cameraIndex);
		Mat frame = new Mat();
		camera.read(frame);
		if (!camera.isOpened()) {
			System.out.println("Can not start webcam index " + cameraIndex);
		} else {
			ThreadCapture capThread = new OpencvFirstStep().new ThreadCapture(camera);
			capThread.start();

			Scanner scanner = new Scanner(System.in);
			while (true) {
				System.out.println("Enter A to capture, or any to stop...");
				String input = scanner.nextLine();
				if ("A".equalsIgnoreCase(input)) {
					capThread.isCapture = true;
				} else {
					capThread.setStop(true);
					break;
				}
			}
			scanner.close();
		}
	}

}
