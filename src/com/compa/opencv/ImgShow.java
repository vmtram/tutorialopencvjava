package com.compa.opencv;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

/**
 * 
 * @author Sea
 *
 */
public class ImgShow {
	public JFrame frame;
	JLabel label;

	public ImgShow() {
		frame = new JFrame();
		label = new JLabel();
		frame.getContentPane().add(label);
	}

	public ImgShow(String title) {
		frame = new JFrame(title);
		label = new JLabel();
		frame.getContentPane().add(label);
	}

	public void show(Mat matFrame) {
		try {
			// Imgproc.resize(matFrame, matFrame, new Size(640, 480));
			MatOfByte matOfByte = new MatOfByte();
			Highgui.imencode(".jpg", matFrame, matOfByte);
			byte[] byteArray = matOfByte.toArray();
			BufferedImage bufImage = null;

			InputStream in = new ByteArrayInputStream(byteArray);
			bufImage = ImageIO.read(in);
			label.setIcon(new ImageIcon(bufImage));
			frame.pack();

			frame.setVisible(true);
		} catch (Exception e) {
			// e.printStackTrace();
		}
	}

	public void show(Mat matFrame, int width, int height) {
		try {
			Imgproc.resize(matFrame, matFrame, new Size(width, height));
			MatOfByte matOfByte = new MatOfByte();
			Highgui.imencode(".jpg", matFrame, matOfByte);
			byte[] byteArray = matOfByte.toArray();
			BufferedImage bufImage = null;

			InputStream in = new ByteArrayInputStream(byteArray);
			bufImage = ImageIO.read(in);
			label.setIcon(new ImageIcon(bufImage));
			frame.pack();
			frame.setVisible(true);
		} catch (Exception e) {
			// e.printStackTrace();
		}
	}
}
